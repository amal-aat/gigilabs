﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCryptTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "BCryptTest";

            string password = "My$ecureP@$sW0Rd";

            //GenerateSaltBenchmarks(password);

            string salt = BCryptHelper.GenerateSalt();
            string hashedPassword = BCryptHelper.HashPassword(password, salt);

            bool valid = BCryptHelper.CheckPassword(password, hashedPassword);

            Console.WriteLine("Salt:            {0}", salt);
            Console.WriteLine("Hashed Password: {0}", hashedPassword);
            Console.WriteLine("Valid:           {0}", valid);

            Console.ReadLine();
        }

        static void GenerateSaltBenchmarks(string password)
        {
            for (int i = 10; i < 16; i++)
            {
                using (var scopedTimer = new ScopedTimer($"GenerateSalt({i})"))
                {
                    string salt = BCryptHelper.GenerateSalt(i);
                    string hashedPassword = BCryptHelper.HashPassword(password, salt);
                }
            }
        }
    }
}
