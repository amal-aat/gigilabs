﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetLogging
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Akka .NET Logging";

            using (var actorSystem = ActorSystem.Create("MyActorSystem"))
            {
                Console.WriteLine("ActorSystem created!");

                var actor = actorSystem.ActorOf(Props.Create<LoggingActor>(), "loggingActor");

                actor.Tell("Hello!");

                Console.WriteLine("Press ENTER to exit...");
                Console.ReadLine();
            }
        }
    }
}
