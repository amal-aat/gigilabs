﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetReceiveTImeout
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ReceiveTimeout Example";

            using (var actorSystem = ActorSystem.Create("MyActorSystem"))
            {
                var actor = actorSystem.ActorOf(
                    Props.Create<SomeActor>(), "SomeActor");

                actorSystem.Scheduler.ScheduleTellRepeatedly(
                    initialDelay: TimeSpan.FromSeconds(1),
                    interval: TimeSpan.FromSeconds(5),
                    receiver: actor,
                    message: "Hello!",
                    sender: ActorRefs.NoSender
                );

                Console.ReadLine();
            }
        }
    }
}
