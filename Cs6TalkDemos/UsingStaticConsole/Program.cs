﻿using System;
using System.Console;

namespace UsingStaticConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Hello world!");
            ReadLine();
        }
    }
}
