﻿using System;
using System.Math;

namespace UsingStaticMath
{
    class Program
    {
        static double CalculateSomething(double angle)
        {
            var numerator = Sin(angle) * Sin(angle) + Cos(angle);
            var denominator = 1 + Sqrt(Sin(angle) + Pow(Cos(angle), 3));

            return Pow(numerator / denominator, 2);
        }

        static void Main(string[] args)
        {
            var result = CalculateSomething(Math.PI / 2);

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
