﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedSingleton
{
    public class DbManager
    {
        private static DbManager instance;

        public static DbManager Instance => instance ?? (instance = new DbManager());

        private DbManager()
        {

        }

        public void Open() => Console.WriteLine("Opened");

        public void Close() => Console.WriteLine("Close");
    }
}
