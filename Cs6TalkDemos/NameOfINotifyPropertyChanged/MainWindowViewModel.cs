﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace NameOfINotifyPropertyChanged
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string currentTime;
        private Timer timer;

        public string Time
        {
            get
            {
                return this.currentTime;
            }
            set
            {
                this.currentTime = value;
                this.OnPropertyChanged(nameof(Time));
            }
        }

        public MainWindowViewModel()
        {
            this.timer = new Timer();
            this.timer.Interval = 1000;
            this.timer.Elapsed += Timer_Elapsed;
            this.timer.AutoReset = true;
            this.timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Time = DateTime.Now.ToString("HH:mm:ss");
        }

        #region INotifyPropertyChanged stuff

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion INotifyPropertyChanged stuff
    }
}
