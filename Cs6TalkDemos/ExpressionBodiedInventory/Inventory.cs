﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedInventory
{
    public class Inventory
    {
        private string[] inventory = new string[10];

        public string this[int index] => this.inventory[index];

        public void Put(int index, string item) =>  this.inventory[index] = item;

        public void RemoveAt(int index) => this.inventory[index] = null;

        public string Get(int index) => this[index];
    }
}
