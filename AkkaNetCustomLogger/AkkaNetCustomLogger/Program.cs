﻿using Akka.Actor;
using Akka.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetCustomLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Akka .NET Custom Logger";

            using (var actorSystem = ActorSystem.Create("MyActorSystem"))
            {
                var logger = Logging.GetLogger(actorSystem, actorSystem, null);
                logger.Info("ActorSystem created!");

                Console.WriteLine("Press ENTER to exit...");
                Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
