﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace AppSettingsFramework.Library.Tests
{
    [TestClass]
    public class ConfigKeyProviderTests
    {
        // Get - int

        [TestMethod]
        public void Get_IntAvailableWithDefault_ValueReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.Read(key)).Returns("5000");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 5000;
            var actual = provider.Get<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Get_IntAvailableWithoutDefault_ValueReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.Read(key)).Returns("5000");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 5000;
            var actual = provider.Get<int>(key);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Get_IntNotAvailableWithDefault_DefaultReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 3000;
            var actual = provider.Get<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Get_IntInvalidWithDefault_DefaultReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.Read(key)).Returns("abc");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 3000;
            var actual = provider.Get<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }
        
        // GetAsync - int

        [TestMethod]
        public async Task GetAsync_IntAvailableWithDefault_ValueReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.ReadAsync(key)).Returns(Task.FromResult("5000"));

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 5000;
            var actual = await provider.GetAsync<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetAsync_IntAvailableWithoutDefault_ValueReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.ReadAsync(key)).Returns(Task.FromResult("5000"));

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 5000;
            var actual = await provider.GetAsync<int>(key);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetAsync_IntNotAvailableWithDefault_DefaultReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 3000;
            var actual = await provider.GetAsync<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetAsync_IntInvalidWithDefault_DefaultReturned()
        {
            // arrange

            var key = "timeoutInMilliseconds";

            var reader = new Mock<IConfigKeyReader>();
            reader.Setup(r => r.ReadAsync(key)).Returns(Task.FromResult("abc"));

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            var expected = 3000;
            var actual = await provider.GetAsync<int>(key, 3000);

            // assert

            Assert.AreEqual(expected, actual);
        }
    }
}
