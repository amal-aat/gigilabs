﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubSubExample
{
    public class Publisher : IPublisher
    {
        private List<ISubscriber> subscribers;

        public Publisher()
        {
            this.subscribers = new List<ISubscriber>();
        }

        public void Subscribe(ISubscriber subscriber)
        {
            this.subscribers.Add(subscriber);
        }

        public void NotifyAll(string message)
        {
            foreach (var subscriber in this.subscribers)
                subscriber.Notify(message);
        }

        public void Unsubscribe(ISubscriber subscriber)
        {
            this.subscribers.Remove(subscriber);
        }
    }
}
