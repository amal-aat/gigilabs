﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetEntityPerChild
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Akka .NET Child Per Entity Example";

            using (var actorSystem = ActorSystem.Create("StockActorSystem"))
            {
                var props = Props.Create<StockCoordinatorActor>();
                var coord = actorSystem.ActorOf(props, "StockCoordinatorActor");

                coord.Tell(new StockUpdate("ABC", 1.20m));
                coord.Tell(new StockUpdate("XYZ", 0.59m));
                coord.Tell(new StockUpdate("ABC", 1.21m));
                coord.Tell(new StockUpdate("HBZ", 0.86m));
                coord.Tell(new StockUpdate("FUK", 1.20m));
                coord.Tell(new StockUpdate("XYZ", 0.57m));

                Console.ReadLine();
            }
        }
    }
}
