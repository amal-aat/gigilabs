﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetEntityPerChild
{
    public class StockCoordinatorActor : ReceiveActor
    {
        public StockCoordinatorActor()
        {
            this.Receive<StockUpdate>(Handle, null);
        }

        private void Handle(StockUpdate update)
        {
            var childName = update.Symbol;

            // check if a child with that name exists
            var child = Context.Child(childName);

            // if it doesn't exist, create it
            if (child == ActorRefs.Nobody)
            {
                var props = Props.Create(() => new StockActor(childName));
                child = Context.ActorOf(props, childName);
            }

            // forward the message to the child actor
            child.Tell(update.Price);
        }
    }
}
