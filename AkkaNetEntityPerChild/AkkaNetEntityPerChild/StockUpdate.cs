﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetEntityPerChild
{
    public class StockUpdate
    {
        public string Symbol { get; }
        public decimal Price { get; }

        public StockUpdate(string symbol, decimal price)
        {
            this.Symbol = symbol;
            this.Price = price;
        }
    }
}
