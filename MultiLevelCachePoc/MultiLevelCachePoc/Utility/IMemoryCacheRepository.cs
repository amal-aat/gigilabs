﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLevelCachePoc.Utility
{
    public interface IMemoryCacheRepository
    {
        Task<List<string>> GetLanguagesAsync();
    }
}
