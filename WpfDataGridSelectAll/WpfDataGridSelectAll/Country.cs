﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataGridSelectAll
{
    public class Country : ObservableObject
    {
        private int id;
        private string name;
        private bool selected;
        private Action<bool> onSelectionChanged;

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.Set(() => Id, ref id, value);
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.Set(() => Name, ref name, value);
            }
        }

        public bool Selected
        {
            get
            {
                return this.selected;
            }
            set
            {
                this.Set(() => Selected, ref selected, value);

                this.onSelectionChanged(value);
            }
        }

        public Country(int id, string name, Action<bool> onSelectionChanged)
        {
            this.id = id;
            this.name = name;
            this.onSelectionChanged = onSelectionChanged;
        }
    }
}
