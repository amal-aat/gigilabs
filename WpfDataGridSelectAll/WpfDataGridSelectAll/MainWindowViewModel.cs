﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataGridSelectAll
{
    public class MainWindowViewModel : ViewModelBase
    {
        private bool allSelected;

        public bool AllSelected
        {
            get
            {
                return this.allSelected;
            }
            set
            {
                this.Set(() => AllSelected, ref allSelected, value);

                foreach (var country in this.Countries)
                    country.Selected = value;
            }
        }

        public ObservableCollection<Country> Countries { get; }

        public MainWindowViewModel()
        {
            var countries = new Country[]
            {
                new Country(1, "Japan", OnCountrySelectionChanged),
                new Country(2, "Italy", OnCountrySelectionChanged),
                new Country(3, "England", OnCountrySelectionChanged),
                new Country(4, "Norway", OnCountrySelectionChanged),
                new Country(5, "Poland", OnCountrySelectionChanged)
            };

            this.Countries = new ObservableCollection<Country>(countries);
        }

        private void OnCountrySelectionChanged(bool value)
        {
            if (allSelected && !value)
            { // all are selected, and one gets turned off
                allSelected = false;
                RaisePropertyChanged(() => this.AllSelected);
            }
            else if (!allSelected && this.Countries.All(c => c.Selected))
            { // last one off one gets turned on, resulting in all being selected
                allSelected = true;
                RaisePropertyChanged(() => this.AllSelected);
            }
        }
    }
}
