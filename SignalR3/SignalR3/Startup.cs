﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Threading;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(SignalR3.Startup))]

namespace SignalR3
{
    public class Startup
    {
        private static Thread stockTickerThread = new Thread(() =>
            {
                var stockTickerHub = GlobalHost.ConnectionManager.GetHubContext<StockTickerHub>();

                while (true)
                {
                    stockTickerHub.Clients.All.update(5);
                    Thread.Sleep(1000);
                }
            });

        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();

            stockTickerThread.Start();
        }
    }
}
