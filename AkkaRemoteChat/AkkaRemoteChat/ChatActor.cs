﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaRemoteChat
{
    public class ChatActor : TypedActor, IHandle<string>
    {
        public ChatActor()
        {
            
        }

        public void Handle(string message)
        {
            using (var colour = new ScopedConsoleColour(ConsoleColor.White))
                Console.WriteLine(message);
        }
    }
}
