﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroIndexingSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            // Build the index

            string[] documents = { "doc1.txt", "doc2.txt", "doc3.txt" };
            Dictionary<string, List<string>> index = new Dictionary<string, List<string>>();

            foreach (string document in documents)
            {
                string documentStr = File.ReadAllText(document);
                string[] words = documentStr.Split();

                foreach (string word in words)
                {
                    if (!index.ContainsKey(word))
                        index[word] = new List<string>();

                    index[word].Add(document);
                }
            }

            // Query the index

            string keyword = Console.ReadLine();

            if (index.ContainsKey(keyword))
            {
                foreach (string document in index[keyword])
                {
                    Console.WriteLine(document);
                }
            }
            else
                Console.WriteLine("Not found!");

            Console.ReadKey(true);
        }
    }
}
