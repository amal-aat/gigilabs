﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameClient.Messages
{
    public class DrawMessage
    {
        public short X { get; }
        public short Y { get; }
        public char Char { get; }

        public DrawMessage(short x, short y, char @char)
        {
            this.X = x;
            this.Y = y;
            this.Char = @char;
        }
    }
}
