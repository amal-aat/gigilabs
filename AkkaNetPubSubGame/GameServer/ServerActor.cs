﻿using Akka.Actor;
using GameMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public class ServerActor : TypedActor,
        IHandle<SubscribeMessage>,
        IHandle<UpdateLocationMessage>,
        IHandle<UnsubscribeMessage>
    {
        private Dictionary<Guid, IActorRef> subscribers;

        public ServerActor()
        {
            this.subscribers = new Dictionary<Guid, IActorRef>();
        }

        public void Handle(SubscribeMessage message)
        {
            this.subscribers[message.SubscriberGuid] = Sender;
        }

        public void Handle(UpdateLocationMessage message)
        {
            this.Broadcast(message);
        }

        public void Handle(UnsubscribeMessage message)
        {
            // remove subscription

            if (this.subscribers.ContainsKey(message.SubscriberGuid))
                this.subscribers.Remove(message.SubscriberGuid);

            // broadcast removal to everyone else

            this.Broadcast(message);
        }

        private void Broadcast<T>(T message) where T : class
        {
            foreach (var subscriber in this.subscribers.Values)
                subscriber.Tell(message);
        }
    }
}
