﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameMessages
{
    public class SubscribeMessage
    {
        public Guid SubscriberGuid { get; }

        public SubscribeMessage(Guid subscriberGuid)
        {
            this.SubscriberGuid = subscriberGuid;
        }
    }
}
