﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAnnotationsIntro
{
    class Program
    {
        static void RunValidateProperty(string value)
        {
            var person = new Person();

            var context = new ValidationContext(person) { MemberName = "Name" };
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateProperty(value, context, results);
        }

        static void RunValidateObject()
        {
            var person = new Person();

            var context = new ValidationContext(person);
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateObject(person, context, results, true);
        }

        static void Main(string[] args)
        {
            RunValidateObject();
        }
    }
}
