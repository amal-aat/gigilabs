﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Levenshtein Distance";

            // input

            Console.Write("Input first string: ");
            string s = Console.ReadLine().ToLowerInvariant();
            Console.Write("Input second string: ");
            string t = Console.ReadLine().ToLowerInvariant();

            // init

            var matrix = new int[s.Length + 1, t.Length + 1];

            for (int i = 0; i < s.Length + 1; i++)
                matrix[i, 0] = i;

            for (int j = 0; j < t.Length + 1; j++)
                matrix[0, j] = j;

            // calculate

            for (int i = 1; i <= s.Length; i++)
            {
                for (int j = 1; j <= t.Length; j++)
                {
                    int cost = s[i - 1] == t[j - 1] ? 0 : 1;

                    int topPlus1 = matrix[i - 1, j] + 1;
                    int leftPlus1 = matrix[i, j - 1] + 1;
                    int topLeftPlusCost = matrix[i - 1, j - 1] + cost;

                    var min = Math.Min(topPlus1, leftPlus1);
                    min = Math.Min(min, topLeftPlusCost);
                    matrix[i, j] = min;
                }
            }

            // visualise

            DrawMatrix(s, t, matrix);

            // output

            int levenshteinDistance = matrix[s.Length, t.Length];
            Console.WriteLine("Levenshtein distance = {0}", levenshteinDistance);
            Console.WriteLine();

            // end

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(false);
        }

        static void DrawMatrix(string s, string t, int[,] matrix)
        {
            Console.WriteLine();
            Console.Write("   ");

            for (int i = 0; i < t.Length; i++)
                Console.Write("{0}", t[i]);
            Console.WriteLine();

            for (int i = 0; i < s.Length + 1; i++)
            {
                Console.Write("{0} ", i > 0 ? s[i - 1] : ' ');

                for (int j = 0; j < t.Length + 1; j++)
                    Console.Write(matrix[i, j]);

                Console.WriteLine();
            }

            Console.WriteLine();
        }
    }
}
