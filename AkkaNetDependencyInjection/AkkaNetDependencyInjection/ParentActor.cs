﻿using Akka.Actor;
using Akka.DI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetDependencyInjection
{
    public class ParentActor : ReceiveActor
    {
        public ParentActor(ILazyAss lazyAss)
        {
            Console.WriteLine("Parent Actor created!");

            var childActorProps = Context.DI().Props<ChildActor>();
            var childActor = Context.ActorOf(childActorProps, "ChildActor");
        }
    }
}
