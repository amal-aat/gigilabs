﻿using Akka.Actor;
using Akka.Persistence;
using AkkaNetPersistenceChess.Actors;
using AkkaNetPersistenceChess.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetPersistenceChess
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Akka .NET Persistence Chess Example";

            using (var actorSystem = ActorSystem.Create("Chess"))
            {
                var drawingProps = Props.Create<ChessBoardDrawingActor>();
                var drawingActor = actorSystem.ActorOf(drawingProps, "DrawingActor");

                Guid gameId = Guid.Parse("F56079D3-4625-409A-B734-C9BDEBA6D7FA");
                var gameProps = Props.Create<ChessGameActor>(gameId, drawingActor);
                var gameActor = actorSystem.ActorOf(gameProps, "GameActor");

                HandleInput(gameActor);

                Console.ReadLine();
            }
        }

        static void HandleInput(IActorRef chessGameActor)
        {
            string input = string.Empty;

            while (input != null) // quit on Ctrl+Z
            {
                input = Console.ReadLine();

                var tokens = input.Split();

                switch (tokens[0]) // check first word
                {
                    case "move": // e.g. move e2 to e4
                        {
                            string from = tokens[1];
                            string to = tokens[3];
                            var message = new MoveMessage(from, to);

                            chessGameActor.Tell(message);
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid command.");
                        break;
                }
            }
        }
    }
}
