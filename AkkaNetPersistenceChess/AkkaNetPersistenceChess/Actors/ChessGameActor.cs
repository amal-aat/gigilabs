﻿using Akka.Actor;
using Akka.Persistence;
using AkkaNetPersistenceChess.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AkkaNetPersistenceChess.Actors
{
    public class ChessGameActor : ReceivePersistentActor
    {
        private Guid gameId;
        private IActorRef rendererActor;

        private char[][] chessBoard = new char[][]
        {
            "rnbqkbnr".ToCharArray(),
            "pppppppp".ToCharArray(),
            "        ".ToCharArray(),
            "        ".ToCharArray(),
            "        ".ToCharArray(),
            "        ".ToCharArray(),
            "PPPPPPPP".ToCharArray(),
            "RNBQKBNR".ToCharArray()
        };

        public override string PersistenceId
        {
            get
            {
                return this.gameId.ToString("N");
            }
        }

        public ChessGameActor(Guid gameId, IActorRef rendererActor)
        {
            this.gameId = gameId;
            this.rendererActor = rendererActor;

            this.RedrawBoard();

            this.Recover<MoveMessage>(RecoverAndHandle, null);
            this.Command<MoveMessage>(PersistAndHandle, null);
        }

        public void RecoverAndHandle(MoveMessage message)
        {
            Handle(message);
            Thread.Sleep(2000);
        }

        public void PersistAndHandle(MoveMessage message)
        {
            Persist(message, persistedMessage => Handle(persistedMessage));
        }

        public void Handle(MoveMessage message)
        {
            var fromPoint = this.TranslateMove(message.From);
            var toPoint = this.TranslateMove(message.To);

            char piece = this.chessBoard[fromPoint.Y][fromPoint.X];

            chessBoard[fromPoint.Y][fromPoint.X] = ' '; // erase old location
            chessBoard[toPoint.Y][toPoint.X] = piece; // set new location

            this.RedrawBoard();
        }

        private Point TranslateMove(string move)
        {
            // e.g. e4: e is the column, and 4 is the row

            char colChar = move[0];
            char rowChar = move[1];

            int col = colChar - 97;
            int row = 8 - (rowChar - '0');

            return new Point(col, row);
        }

        private void RedrawBoard()
        {
            var drawMessage = new DrawChessBoardMessage(this.chessBoard);
            this.rendererActor.Tell(drawMessage);
        }
    }
}
