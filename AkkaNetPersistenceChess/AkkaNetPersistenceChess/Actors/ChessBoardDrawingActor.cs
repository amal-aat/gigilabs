﻿using Akka.Actor;
using AkkaNetPersistenceChess.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetPersistenceChess.Actors
{
    public class ChessBoardDrawingActor : ReceiveActor
    {
        public ChessBoardDrawingActor()
        {
            this.Receive<DrawChessBoardMessage>(m => Handle(m));
        }

        public void Handle(DrawChessBoardMessage message)
        {
            Console.Clear();

            var chessBoard = message.ChessBoard;

            for (int i = 0; i < chessBoard.Length; i++)
            {
                var row = chessBoard[i];

                // draw grid numbers

                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
                Console.Write($"{8 - i}");
                Console.ResetColor();
                Console.Write(" ");

                // draw row

                foreach (var square in row)
                    Console.Write($"{square} ");

                Console.WriteLine();
            }

            // draw grid letters

            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("  ");
            for (int i = 0; i < 8; i++)
            {
                Console.Write($"{(char)('a' + i)} ");
            }
            Console.ResetColor();

            Console.WriteLine();
        }
    }
}
