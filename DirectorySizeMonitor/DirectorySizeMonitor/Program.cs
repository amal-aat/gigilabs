﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DirectorySizeMonitor
{
    class Program
    {
        private static string directoryPath;
        private static long threshold;

        static void Main(string[] args)
        {
            try
            {
                Console.Title = "Directory Size Monitor";

                directoryPath = ConfigurationManager.AppSettings["DirectoryPath"];
                threshold = Convert.ToInt64(ConfigurationManager.AppSettings["Threshold"]);

                var timer = new Timer();
                timer.Interval = 5000;
                timer.Elapsed += Timer_Elapsed;
                timer.Enabled = true;
                timer.Start();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey(true);
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var dir = new DirectoryInfo(directoryPath);
            var size = CalculateDirectorySize(dir);
            string warning = size > threshold ? " WARNING - THRESHOLD EXCEEDED" : null;
            Console.WriteLine("{0}{1}", size, warning);
        }

        static long CalculateDirectorySize(DirectoryInfo dir)
        {
            long totalSize = 0L;

            foreach (var file in dir.EnumerateFiles())
                totalSize += file.Length;

            foreach (var subdir in dir.EnumerateDirectories())
                totalSize += CalculateDirectorySize(subdir);

            return totalSize;
        }
    }
}
